function star(x, y, angle, innerR, outerR, spikes, alpha, dx, dy, d0, dalpha) {
  this.x = x;
  this.y = y;
  this.angle = angle;
  this.innerR = innerR;
  this.outerR = outerR;
  this.spikes = spikes;
  this.alpha = alpha;
  this.dx = dx;
  this.dy = dy;
  this.d0 = d0;
  this.dalpha = dalpha;
}

star.prototype = {
  draw : function(canvasContext) {
    var nX = this.x;
    var nY = this.y;
    var nAngle = this.angle;

    canvasContext.moveTo(nX, nX - this.outerR);
    canvasContext.beginPath();

    for (var i = 0; i < this.spikes; i++) {
      nX = this.x + Math.cos(nAngle) * this.outerR;
      nY = this.y + Math.sin(nAngle) * this.outerR;
      canvasContext.lineTo(nX, nY);
      nAngle += Math.PI / this.spikes;
      nX = this.x + Math.cos(nAngle) * this.innerR;
      nY = this.y + Math.sin(nAngle) * this.innerR;
      canvasContext.lineTo(nX, nY);
      nAngle += Math.PI / this.spikes;
    }
    nX = this.x + Math.cos(nAngle) * this.outerR;
    nY = this.y + Math.sin(nAngle) * this.outerR;
    canvasContext.lineTo(nX, nY);
    canvasContext.strokeStyle = "rgba(0, 0, 0, 0)";
    canvasContext.stroke();
    canvasContext.closePath();
    canvasContext.fillStyle = "white";
    canvasContext.globalAlpha = this.alpha;
    canvasContext.fill();
  },
  update : function(canvas, canvasContext) {
    this.x = this.x + this.dx;
    this.y = this.y + this.dy;
    this.angle = this.angle + this.d0;
    this.alpha = this.alpha + this.dalpha;
    if((this.x - this.outerR - this.dx) > canvas.width)
      this.x = - this.outerR - this.dx;
    if((this.y - this.outerR - this.dy) > canvas.height)
      this.y = - this.outerR - this.dy;
    if((this.x + this.outerR + this.dx) < 0)
      this.x = canvas.width + this.outerR + this.dx;
    if((this.y + this.outerR + this.dy) < 0)
      this.y = canvas.height + this.outerR + this.dy;
    if(this.alpha >= 1 || this.alpha <= 0)
      this.dalpha = -this.dalpha;
    this.draw(canvasContext);
  }
}

var c = document.getElementById("cnvs");
var cctx = c.getContext("2d");
c.width = window.innerWidth;
c.height = window.innerHeight;

function init() {

}

function update() {
  cctx.clearRect(0, 0, c.width, c.height);

  window.requestAnimationFrame(update);
}

init();
update();
