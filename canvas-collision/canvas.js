function circle(x, y, r, dx, dy, color) {
  this.x = x;
  this.y = y;
  this.r = r;
  this.dx = dx;
  this.dy = dy;
  this.color = color;
}

circle.prototype = {
  drawCircle : function(cctx) {
    cctx.beginPath();
    cctx.strokeStyle = this.color;
    cctx.fillStyle = this.color;
    cctx.arc(this.x, this.y, this.r, 0, 2*Math.PI);
    cctx.stroke();
    cctx.fill();
  },
  updateCircle : function(cctx) {
    this.x = this.x + this.dx;
    this.y = this.y + this.dy;
    this.drawCircle(cctx);
  }
}

var NO_OF_BALLS = 20;
var CELL_SIZE = 80;
var MIN_RADIUS = 15;
var MAX_RADIUS = 45;
var MIN_SPEED = 0.005;
var MAX_SPEED = 0.015;

var c = document.getElementById("cnvs");
var cctx = c.getContext("2d");
c.width = window.innerWidth;
c.height = window.innerHeight;

var hash = new hash2d(c.width, c.height, CELL_SIZE);
var circles = [];

function getColor() {
  var colors = ["#F44336","#E91E63","#9C27B0","#673AB7","#795548",
                "#3F51B5","#0D47A1","#2196F3","#03A9F4","#00BCD4",
                "#009688","#4CAF50","#8BC34A","#00C853","#CDDC39",
                "#FFEB3B","#FFC107","#FF9800","#FF5722","#D50000"];
  return colors[(circles.length % NO_OF_BALLS)];
}

function isOverlap(cir) {
  var cirList = hash.nearby(cir);
  for(let nearCic of cirList)
    if(distance(nearCic.x, nearCic.y, cir.x, cir.y) <= nearCic.r + cir.r)
      return true;
  return false;
}

function distance(x1, y1, x2, y2) {
  return Math.sqrt(Math.pow(x2 - x1, 2)+Math.pow(y2 - y1, 2));
}

function checkWallCollision(cir) {
  if(cir.x + cir.dx > c.width - cir.r || cir.x + cir.dx < cir.r)
    cir.dx = -cir.dx;
  if(cir.y + cir.dy > c.height - cir.r || cir.y + cir.dy < cir.r)
    cir.dy = -cir.dy;
}

function checkInterCollision(cir) {
  var cirList = hash.nearby(cir);
  for(let nearCic of cirList) {
    if(distance(nearCic.x + nearCic.dx, nearCic.y + nearCic.dy,
        cir.x + cir.dx, cir.y + cir.dy) <= nearCic.r + cir.r) {
      //ForNow lets keep mass constant
      var tempx = cir.dx;
      var tempy = cir.dy;
      cir.dx = nearCic.dx;
      cir.dy = nearCic.dy;
      nearCic.dx = tempx;
      nearCic.dy = tempy;
      return;
    }
  }
}

function init() {
  for (var i = 0; i < NO_OF_BALLS; i++) {
    var x = Math.floor(Math.random() * c.width);
    var y = Math.floor(Math.random() * c.height);
    var r = MIN_RADIUS + Math.floor(Math.random() * (MAX_RADIUS-MIN_RADIUS));
    var dx = c.width * MIN_SPEED
              - Math.floor(Math.random() * c.width * (MAX_SPEED-MIN_SPEED));
    var dy = c.height * MIN_SPEED
              - Math.floor(Math.random() * c.height * (MAX_SPEED-MIN_SPEED));
    var color = getColor();
    var cir = new circle(x, y, r, dx, dy, color);

    if(x < r || x > (c.width - r) || y < r || y > (c.height - r) ||
      isOverlap(cir)) {
      i--; continue;
    }
    cir.drawCircle(cctx);
    circles.push(cir);
    hash.register(cir);
  }
}

function update() {
  hash.clear();
  cctx.clearRect(0, 0, c.width, c.height);
  for(var i=0; i<circles.length; i++) {
    checkWallCollision(circles[i]);
    checkInterCollision(circles[i]);
    circles[i].updateCircle(cctx);
    hash.register(circles[i]);
  }
  window.requestAnimationFrame(update);
}

init();
window.requestAnimationFrame(update);
