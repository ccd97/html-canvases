function hash2d(width, height, cellsize) {
  this.width = width;
  this.height = height;
  this.cellsize = cellsize;
  this.buckets = {};

  this.cols = Math.ceil(width / cellsize);
  this.rows = Math.ceil(height / cellsize);

  for(var i=0; i<this.cols * this.rows; i++)
    this.buckets[i] = [];
}

hash2d.prototype = {

  clear : function() {
    this.buckets = {};

    for(var i=0; i<this.cols * this.rows; i++)
      this.buckets[i] = [];
  },

  add : function(x, y, bucket) {
    var cellPosition =  Math.floor(x / this.cellsize) +
                        Math.floor(y / this.cellsize) *
                        this.cols;

    bucket.add(cellPosition);
  },

  getId : function(cir) {
    var bucket = new Set();

    this.add(cir.x - cir.r, cir.y - cir.r, bucket);
    this.add(cir.x + cir.r, cir.y - cir.r, bucket);
    this.add(cir.x + cir.r, cir.y + cir.r, bucket);
    this.add(cir.x - cir.r, cir.y + cir.r, bucket);

    return bucket;
  },

  register : function(cir) {
    var id = this.getId(cir);

    for(let i of id)
      this.buckets[i].push(cir);
  },

  nearby : function(cir) {
    var ccls = new Set();
    var id = this.getId(cir);

    for(let i of id) {
      var bkt = this.buckets[i];
      for(let j of bkt) {
        ccls.add(j);
      }
    }
    ccls.delete(cir);

    return ccls;
  }
}
